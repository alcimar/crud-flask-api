texto = "001.047.114-69 | Alcimar Álvaro Costa!"

# Dividindo o texto com base no caractere |
partes = texto.split('|')

# Salvando os resultados nas variáveis
uuid_texto = partes[0].strip()
texto_a_resumir = partes[1].strip()

print(uuid_texto)
print(texto_a_resumir)